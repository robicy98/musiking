create database if not exists bands_database;

use bands_database;

-- drop table PersonsBand;
-- drop table PersonsInstrument;
-- drop table Instruments;
-- drop table Bands;
-- drop table Users;

create table if not exists Users (
	UserID int NOT NULL AUTO_INCREMENT,
    Username varchar(100),
    FirstName varchar(100),
    LastName varchar(100),
    Rule varchar(100),
    Email varchar(100),
    UserPassword varchar(200),
    Picture varchar(100),
    primary key (UserID)
);

create table if not exists Bands (
	BandID int NOT NULL AUTO_INCREMENT,
    BandType varchar(100),
    Contry varchar(100),
    City varchar(100),
    BandName varchar(100),
    BandMaster int,
    Picture varchar(100),
    PRIMARY KEY (BandID),
    FOREIGN KEY (BandMaster) REFERENCES Users(UserID)
);

create table if not exists Instruments (
	InstrumentID int not null auto_increment,
    InstrumentName varchar(100),
	PRIMARY KEY (InstrumentID)
);

create table if not exists PersonsInstrument(
	UserID int,
    InstrumentID int,
	FOREIGN KEY (UserID) REFERENCES Users(UserID),
    FOREIGN KEY (InstrumentID) REFERENCES Instruments(InstrumentID)
);

create table if not exists PersonsBand(
	UserID int,
    BandID int,
	FOREIGN KEY (UserID) REFERENCES Users(UserID),
    FOREIGN KEY (BandID) REFERENCES Bands(BandID)
);


insert into Users(Username,Firstname, Lastname, Rule, Email, UserPassword, Picture)
values ("admin","Admin", "Admin", "Admin", "admin@gmail.com", "Admin1234", "");
insert into Users(Username,Firstname, Lastname, Rule, Email, UserPassword, Picture)
values ("csikota","Csikota", "Jozsef", "BandMaster", "csikota@gmail.com", "csikota", "");
insert into Users(Username,Firstname, Lastname, Rule, Email, UserPassword, Picture)
values ("deerobert","Dee", "Robert", "BandMaster", "deerobert@gmail.com", "dee", "");
insert into Users(Username,Firstname, Lastname, Rule, Email, UserPassword, Picture)
values ("arpad","Sandor", "Arpad", "BandMaster", "arpi@gmail.com", "arpi", "");
insert into Users(Username,Firstname, Lastname, Rule, Email, UserPassword, Picture)
values ("robert","Szabo", "Robert", "Player", "robert@gmail.com", "robert", "");

insert into Bands(BandType, Contry, City, BandName, BandMaster,Picture)
values ("Brass", "Romania", "Oradea", "Karaszfanband", 3, "");
insert into Bands(BandType, Contry, City, BandName, BandMaster,Picture)
values ("Brass", "Romania", "Csikszentsimon", "Csíkszentsimoni Ifjúsági Koncertfúvós zenekar", 4, "");
insert into Bands(BandType, Contry, City, BandName, BandMaster,Picture)
values ("Concert", "Hungary", "Makó", "Makói koncert zenekar", 2, "");

insert into Instruments (InstrumentName)
values ("Tuba");

insert into PersonsInstrument (UserID, InstrumentID)
values (5, 1);

insert into PersonsBand(UserID, BandID)
values (5,1);

select * from Bands
where bandName like "K%";

ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '1234'