const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const fs = require('fs');
const eformidable = require('express-formidable');

const indexRouter = require('./API/index');
const usersRouter = require('./API/users');
const bandsRouter = require('./API/bands');
const loginRouter = require('./routes/login');
const mydataRouter = require('./routes/mydata');
const usersforadmin = require('./routes/usersforadmin');
const bandList = require('./routes/bandList');
const membesRouter = require('./API/membership');

const app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

const uploadDir = path.join(__dirname, 'file-uploads');
if (!fs.existsSync(uploadDir)) {
  fs.mkdirSync(uploadDir);
}

app.use(eformidable({ uploadDir }));
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/bands', bandsRouter);
app.use('/login', loginRouter);
app.use('/getmydata', mydataRouter);
app.use('/getusers', usersforadmin);
app.use('/getbands', bandList);
app.use('/members', membesRouter);
app.use('/api/users', usersRouter);

app.use((req, res, next) => {
  next(createError(404));
});

app.use((err, req, res) => {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
