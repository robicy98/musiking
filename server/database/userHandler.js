const { pool } = require('../connection/Connection');

exports.findAll = () => new Promise((resolve, reject) => {
  pool.query('SELECT * FROM Users', (err, users) => {
    if (err) {
      reject(err);
    }
    resolve(users);
  });
});

exports.findLikeFirstname = (firsname) => new Promise((resolve, reject) => {
  pool.query(`SELECT * FROM Users where FirstName like ${firsname}`, (err, users) => {
    if (err) {
      reject(err);
    }
    resolve(users);
  });
});

exports.findAllCompact = () => new Promise((resolve, reject) => {
  pool.query('SELECT UserID,Firstname,Lastname,Rule FROM Users', (err, users) => {
    if (err) {
      reject(err);
    }
    resolve(users);
  });
});

exports.findByUserID = (UserID, callback) => {
  pool.query('SELECT * FROM Users WHERE UserID = ?', [UserID], (err, users) => {
    if (err) {
      console.log(`Unable to call error: ${err}`);
    } else {
      callback(users[0]);
    }
  });
};

exports.findByUsername = (Username) => new Promise((resolve, reject) => {
  pool.query('SELECT * FROM Users WHERE Username = ?', [Username], (err, users) => {
    if (err) {
      reject(err);
    }
    resolve(users);
  });
});

exports.addUser = (Username, Firstname, Lastname, Rule, Email, UserPassword, Picture, callback) => {
  pool.query('insert into Users(Username, Firstname, Lastname, Rule, Email, UserPassword, Picture) values (?,?,?,?,?,?,?);',
    [Username, Firstname, Lastname, Rule, Email, UserPassword, Picture], (err) => {
      if (err) {
        console.log(`Unable to call error: ${err}`);
      } else {
        callback();
      }
    });
};

exports.deleteUser = (UserID) => new Promise((resolve, reject) => {
  pool.query('delete from Users WHERE UserID = ?', [UserID], (err, users) => {
    if (err) {
      reject(err);
    }
    resolve(users);
  });
});

exports.updateUser = (UserID, User) => new Promise((resolve, reject) => {
  pool.query(`update Users 
              set ?
              where UserID = ?`, [User, UserID], (err, users) => {
    if (err) {
      reject(err);
    }
    resolve(users);
  });
});
