const { pool } = require('../connection/Connection');

exports.findAll = () => new Promise((resolve, reject) => {
  pool.query('SELECT * FROM Bands', (err, users) => {
    if (err) {
      reject(err);
    }
    resolve(users);
  });
});

exports.findAllCompact = () => new Promise((resolve, reject) => {
  pool.query('SELECT BandID,Contry,BandName FROM Bands', (err, users) => {
    if (err) {
      reject(err);
    }
    resolve(users);
  });
});


exports.findByBandID = (bandID, callback) => {
  pool.query('SELECT * FROM Bands WHERE BandID = ?', [bandID], (err, bands) => {
    if (err) {
      console.log(`Unable to call error: ${err}`);
    } else {
      callback(bands[0]);
    }
  });
};

exports.findLikeName = (bandName, callback) => {
  pool.query(`SELECT * FROM Bands WHERE BandName like "${bandName}%"`, (err, bands) => {
    if (err) {
      console.log(`Unable to call error: ${err}`);
    } else {
      callback(bands);
    }
  });
};

exports.findLikeCountry = (countryName, callback) => {
  pool.query(`SELECT * FROM Bands WHERE Contry like "${countryName}%"`, (err, bands) => {
    if (err) {
      console.log(`Unable to call error: ${err}`);
    } else {
      callback(bands);
    }
  });
};

exports.findLikeCountryCompact = (countryName) => new Promise((resolve, reject) => {
  pool.query(`SELECT BandID,Contry,BandName FROM Bands WHERE Contry like "${countryName}%"`, (err, users) => {
    if (err) {
      reject(err);
    }
    resolve(users);
  });
});
