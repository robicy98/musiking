const { pool } = require('../connection/Connection');

exports.getMemberOfBand = (UserID, BandID) => new Promise((resolve, reject) => {
  pool.query(`select * from PersonsBand
              where PersonsBand.UserID = ${UserID} and
              PersonsBand.BandID = ${BandID}`, (err, users) => {
    if (err) {
      reject(err);
    }
    resolve(users);
  });
});

exports.getPlayBands = (UserID) => new Promise((resolve, reject) => {
  pool.query(`select * from PersonsBand
              where PersonsBand.UserID = ${UserID}`, (err, users) => {
    if (err) {
      reject(err);
    }
    resolve(users);
  });
});

exports.deleteBandMember = (UserID, BandID) => new Promise((resolve, reject) => {
  pool.query(`DELETE FROM PersonsBand 
              where PersonsBand.UserID = ${UserID} and
              PersonsBand.BandID = ${BandID}`, (err, users) => {
    if (err) {
      reject(err);
    }
    resolve(users);
  });
});

exports.addBandMember = (UserID, BandID) => new Promise((resolve, reject) => {
  pool.query(`insert into PersonsBand(UserID, BandID)
              values (${UserID},${BandID});`, (err, users) => {
    if (err) {
      reject(err);
    }
    resolve(users);
  });
});
