const express = require('express');
const jwt = require('jsonwebtoken');
const userHandler = require('../database/userHandler');

const secretKey = '18MDRzJOWpXO6dp7q5lYwi7YvzG5WUHz1H5vyGD2';

const router = express.Router();

router.get('/', (req, res) => {
  const auth = req.headers.authorization;
  if (auth) {
    const token = auth.split(' ')[1];
    jwt.verify(token, secretKey, (err, payload) => {
      if (payload) {
        if (payload.data.rule === 'Admin') {
          userHandler.findAllCompact()
            .then((users) => {
              res.status(200).json(users);
            });
        } else {
          res.status(401).send('You are not logged in properly');
        }
      } else {
        res.status(401).send('You are not logged in properly');
      }
    });
  } else {
    res.status(401).send('You are not logged in properly');
  }
});

module.exports = router;
