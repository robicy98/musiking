const express = require('express');
const bandsHandler = require('../database/bandsHandler');

const router = express.Router();

router.get('/', (req, res) => {
  bandsHandler.findAllCompact().then((result) => res.status(200).json(result));
});

router.get('/country/:name', (req, res) => {
  const { name } = req.params;
  bandsHandler.findLikeCountryCompact(name).then((result) => {
    res.status(200).json(result);
  });
});

module.exports = router;
