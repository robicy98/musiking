const express = require('express');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const userHandler = require('../database/userHandler');

const secretKey = '18MDRzJOWpXO6dp7q5lYwi7YvzG5WUHz1H5vyGD2';
const timeToExpire = '7d';

const hashSize = 32,
  hashAlgorithm = 'sha512',
  iterations = 1000;

const router = express.Router();

function validate(password, hashWithSalt) {
  return new Promise((resolve, reject) => {
    const expectedHash = hashWithSalt.substring(0, hashSize * 2),
      salt = Buffer.from(hashWithSalt.substring(hashSize * 2), 'hex');
    const binaryHash = crypto.pbkdf2Sync(password, salt, iterations, hashSize, hashAlgorithm);
    const actualHash = binaryHash.toString('hex');

    if (expectedHash === actualHash) {
      resolve(true);
    } else {
      reject(console.log('Wrong Password'));
    }
  });
}

router.post('/', (req, res) => {
  const user = {
    username: req.fields.username,
    password: req.fields.password,
  };

  userHandler.findByUsername(user.username)
    .then((users) => {
      validate(user.password, users[0].UserPassword)
        .then(() => {
          const data = {
            username: users[0].Username,
            rule: users[0].Rule,
          };
          const token = jwt.sign({ data }, secretKey, { expiresIn: timeToExpire });
          res.status(200).json(token);
        })
        .catch(() => res.sendStatus(403));
    }).catch(() => res.sendStatus(403));
});

module.exports = router;
