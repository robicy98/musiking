const express = require('express');
const jwt = require('jsonwebtoken');
const userHandler = require('../database/userHandler');

const secretKey = '18MDRzJOWpXO6dp7q5lYwi7YvzG5WUHz1H5vyGD2';

const router = express.Router();

router.get('/:name', (req, res) => {
  const { name } = req.params;

  const auth = req.headers.authorization;
  if (auth) {
    const token = auth.split(' ')[1];
    jwt.verify(token, secretKey, (err, payload) => {
      if (payload) {
        console.log(payload);
        if (payload.data.username === name) {
          userHandler.findByUsername(name)
            .then((users) => {
              res.status(200).json(users);
            });
        } else {
          res.status(401).send('You are not logged in properly');
        }
      } else {
        res.status(402).send('You are not logged in properly');
      }
    });
  } else {
    res.status(403).send('You are not logged in properly');
  }
});

module.exports = router;
