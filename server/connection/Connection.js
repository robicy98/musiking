const mysql = require('mysql');

exports.pool = mysql.createPool({
  connectionLimit: 10,
  database: 'bands_database',
  host: 'localhost',
  port: 3306,
  user: 'root',
  password: '1234',
});
