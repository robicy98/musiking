const express = require('express');

const memberHandler = require('../database/membershipHandler');

const router = express.Router();

router.get('/:UserID', (req, res) => {
  const { UserID } = req.params;
  memberHandler.getPlayBands(UserID)
    .then((result) => res.status(200).json(result))
    .catch(() => {
      res.sendStatus(404);
    });
});

router.post('/', (req, res) => {
  const { UserID, BandID } = req.fields;
  memberHandler.addBandMember(UserID, BandID)
    .then((result) => res.status(200).json(result))
    .catch(() => res.sendStatus(404));
});

router.delete('/', (req, res) => {
  const { UserID, BandID } = req.fields;
  memberHandler.deleteBandMember(UserID, BandID)
    .then((result) => res.status(200).json(result))
    .catch(() => res.sendStatus(404));
});

module.exports = router;
