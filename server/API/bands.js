const express = require('express');

const bandHandler = require('../database/bandsHandler');

const router = express.Router();

router.get('/', (req, res) => {
  bandHandler.findAll((result) => {
    res.status(200).json(result);
  }).catch((err) => {
    res.status(404).json(err);
  });
});

router.get('/:id', (req, res) => {
  let { id } = req.params;
  if (parseInt(id, 10)) {
    id =  parseInt(id, 10);
    bandHandler.findByBandID(id, (result) => {
      res.status(200).json(result);
    });
  } else {
    bandHandler.findLikeName(id, (result) => {
      res.status(200).json(result);
    });
  }
});

router.get('/country/:name', (req, res) => {
  const { name } = req.params;
  bandHandler.findLikeCountry(name, (result) => {
    res.status(200).json(result);
  });
});

module.exports = router;
