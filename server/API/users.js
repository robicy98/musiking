const express = require('express');
const crypto = require('crypto');

const userHandler = require('../database/userHandler');

const hashSize = 32,
  saltSize = 16,
  hashAlgorithm = 'sha512',
  iterations = 1000;

const router = express.Router();

function encrypt(password) {
  return new Promise((resolve) => {
    const salt = crypto.randomBytes(saltSize);
    const hash = crypto.pbkdf2Sync(password, salt, iterations, hashSize, hashAlgorithm);
    const hashWithSalt = Buffer.concat([hash, salt]).toString('hex');
    resolve(hashWithSalt);
  });
}

router.get('/', (req, res) => {
  const { firstname } = req.params;

  if (firstname !== undefined) {
    userHandler.findLikeFirstname(firstname).then((users) => {
      res.status(200).json(users);
    }).catch(() => {
      res.status(404);
    });
  } else {
    userHandler.findAll().then((users) => {
      res.status(200).json(users);
    }).catch((err) => {
      res.status(500).json(err);
    });
  }
});

router.get('/:id', (req, res) => {
  let { id } = req.params;
  if (parseInt(id, 10)) {
    id =  parseInt(id, 10);
    userHandler.findByUserID(id, (users) => {
      res.status(200).json(users);
    });
  } else {
    userHandler.findByUsername(id).then((user) => res.status(200).json(user));
  }
});

router.post('/', (req, res) => {
  const {
    username, cpassword, password, email, image, firstname, lastname,
  } = req.fields;
  if (password !== cpassword) {
    res.status(403).statusMessage('passwords don\'t match').send();
    return;
  }
  encrypt(password).then((hashedPass) => {
    userHandler.addUser(username, firstname, lastname, 'Player', email, hashedPass, image, () => {
      res.status(200).redirect('/');
    });
  });
});

router.delete('/:id', (req, res) => {
  let { id } = req.params;
  if (parseInt(id, 10)) {
    id = parseInt(id, 10);
    userHandler.deleteUser(id).then(() => {
      res.sendStatus(200);
    }).catch(() => {
      res.sendStatus(300);
    });
  }
});

router.put('/', (req, res) => {
  let UserId;
  const {
    username, email, image, firstname, lastname,
  } = req.fields;
  userHandler.findByUsername(username).then((user) => {
    UserId = user[0].UserID;
  }).catch(() => {
    res.sendStatus(404);
  });
  let user;
  if (username) user.Username = username;
  if (firstname) user.FirstName = firstname;
  if (lastname) user.LastName = lastname;
  if (email) user.Email = email;
  if (image) user.Picture = image;
  userHandler.updateUser(UserId, user).then(() => {
    res.sendStatus(200);
  }).catch(() => {
    res.sendStatus(404);
  });
});

module.exports = router;
