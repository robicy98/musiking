// eslint-disable-next-line no-unused-vars
function textscroll() {
  const dir = document.getElementById('direction').value;
  const text = document.getElementById('textarea').value;
  const scrolldiv = document.getElementById('scrollText');
  const towrite = document.getElementById('texthere');
  let newtext = text;
  scrolldiv.classList.remove('lr');
  scrolldiv.classList.remove('rl');
  const t = parseInt(18 / text.length, 10) - 2;
  // eslint-disable-next-line no-plusplus
  for (let i = 0; i < t; i++) {
    newtext += ` ${text}`;
  }
  towrite.innerHTML = newtext;
  if (dir === 'r') {
    scrolldiv.classList.add('lr');
  } else {
    scrolldiv.classList.add('rl');
  }
}

// eslint-disable-next-line no-unused-vars
function getTextWidth(text, font) {
  const canvas = document.createElement('canvas');
  const context = canvas.getContext('2d');
  context.font = font;
  const metrics = context.measureText(text);
  return metrics.width;
}

// eslint-disable-next-line no-unused-vars
window.addEventListener('load', () => {
  const direction = this.document.getElementById('direction');
  const restart = this.document.getElementById('restart');
  const scrollDiv = this.document.getElementById('scrollDiv');
  const scrollText = this.document.getElementById('scrollText');
  const textArea = this.document.getElementById('textarea');

  const width = scrollDiv.clientWidth;
  const left = scrollDiv.offsetLeft;

  let text = textArea.value;
  let moving = 0;
  let startPosition = 0;
  if (direction.value === 'l') {
    moving = 3;
    startPosition = -left;
  } else {
    moving = -3;
    startPosition = left;
  }

  while (text.length < width) {
    text += ` ${textArea.value}`;
  }
  scrollText.innerHTML = text;

  textArea.addEventListener('keydown', () => {
    text = textArea.value;
    while (text.length < width) {
      text += ` ${textArea.value}`;
    }
    scrollText.innerHTML = text;
  }, true);

  restart.addEventListener('click', () => {
    if (moving > 0) {
      startPosition = 0;
    } else {
      startPosition = width;
    }
  }, true);
  direction.addEventListener('change', () => {
    if (direction.value === 'l') {
      moving = 3;
      startPosition = -left;
    } else {
      moving = -3;
      startPosition = left;
    }
  }, true);

  function scrolling() {
    if (moving < 0) {
      if (startPosition > -width) {
        startPosition += moving;
      } else {
        startPosition = width;
      }
      scrollText.style.left = `${startPosition}px`;
    }
    if (moving > 0) {
      if (startPosition < width) {
        startPosition += moving;
      } else {
        startPosition = -left;
      }
      scrollText.style.left = `${startPosition}px`;
    }
  }
  this.setInterval(scrolling, 60);
}, true);
