// eslint-disable-next-line no-unused-vars
function emailValidator() {
  const email = document.getElementById('email');
  const e1 = /[A-Za-z0-9._-]+@gmail\.com/;
  const e2 = /[A-Za-z0-9._-]*@yahoo\.[a-z]+/;
  if (!(e1.test(email.value)) && !(e2.test(email.value))) {
    document.getElementById('Error').innerHTML = 'Invalid Email';
    return false;
  }
  return true;
}

// eslint-disable-next-line no-unused-vars
function webValidator() {
  const web = document.getElementById('favoriteweb');
  const regexp =    /https:\/\/[A-Za-z0-9._-]+.[A-Za-z0-9._-]+.[A-Za-z0-9._-]+.[a-z]{3}/;
  if (!regexp.test(web.value)) {
    document.getElementById('Error').innerHTML = 'Invalid webpage';
    return false;
  }
  return true;
}

// eslint-disable-next-line no-unused-vars
function addFooter() {
  const footer = document.getElementById('footer');
  footer.textContent = document.lastModified;
}

function specialNum(pass) {
  let count = 0;
  // eslint-disable-next-line no-plusplus
  for (let i = 0; i < pass.length; i++) {
    if (/[^0-9a-zA-Z\d\s:]/.test(pass.charAt(i))) {
      // eslint-disable-next-line no-plusplus
      count++;
    }
  }
  return count;
}

// eslint-disable-next-line no-unused-vars
function passwordValiadator() {
  const pass = document.getElementById('password').value;
  const passError = document.getElementById('Error');
  document.getElementById('passverification').innerHTML = pass;
  if (pass.length < 5) {
    passError.innerHTML = 'Min. 5 character';
    return false;
  }
  if (pass.length > 12) {
    passError.innerHTML = 'Max. 12 character';
    return false;
  }
  if (!(/[a-z]+/.test(pass))) {
    passError.innerHTML = 'Min. 1 Lowercase';
    return false;
  }
  if (!(/[A-Z]+/.test(pass))) {
    passError.innerHTML = 'Min. 1 Uppercase';
    return false;
  }
  if (!(/[0-9]+/.test(pass))) {
    passError.innerHTML = 'Min. 1 number';
    return false;
  }
  if (specialNum(pass) === 0) {
    passError.innerHTML = 'Min. 1 special character';
    return false;
  }
  if (specialNum(pass) > 2) {
    passError.innerHTML  = 'Max. 2 special characters';
    return false;
  }
  return true;
}

// eslint-disable-next-line no-unused-vars
function validateForm() {
  document.getElementById('invalid').style.visibility = 'hidden';
  document.getElementById('valid').style.visibility = 'hidden';
  const val = document.getElementById('validateButton');
  if (!emailValidator()) {
    document.getElementById('invalid').style.visibility = 'visible';
    document.getElementById('invalid').style.opacity = 1;
  } else if (!webValidator()) {
    document.getElementById('invalid').style.visibility = 'visible';
    document.getElementById('invalid').style.opacity = 1;
  } else if (!passwordValiadator()) {
    document.getElementById('invalid').style.visibility = 'visible';
    document.getElementById('invalid').style.opacity = 1;
  } else {
    document.getElementById('valid').style.visibility = 'visible';
    document.getElementById('valid').style.opacity = 1;
    val.classList.add('hidden');
    // eslint-disable-next-line func-names
    setTimeout(function () {
      this.document.getElementById('submit').classList.remove('hidden');
      this.valid.classList.add('hidden');
    }, 3000);
  }
}
