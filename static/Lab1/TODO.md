[] Cím, legalább 3 különböző szócikk (article) (tetszőleges szöveggel)
[] Minden article esetében cím, alcím és legalább két szekció (section)
[] Egy lábléc (footer) amely a szerző azonosító információit tartalmazza
[] Legalább 3 külső lapra való hivatkozás, melyek közt legyen relatív és abszolút elérési úttal megadott, és legalább 2 ezek közül olyan HTML oldalra mutasson ami szintén a gyakorlat része. A hivatkozások egy nav elemen belül legyenek elhelyezve.
[] Hivatkozás, melynek segítségével egy az oldalon belüli részhez ugorhatunk (pl. lap tetején a HTML oldal egyes részeihez való ugrási lehetőség, illetve vissza).
[] Lista (egy sorszámozott és egy sorszám nélküli).
[] Táblázat.
[] Kép, melyre rákattintva az illető kép nagyított változatát kapjuk.
[] Arra vonatkozó kommentek, hogy a második résznél szereplő követelmények melyik részét hol használtuk.
