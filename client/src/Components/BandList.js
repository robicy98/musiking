import React, { Component } from 'react'
import { serverURL } from '../props'
import BandElement from './BandElement'
const axios = require('axios') 

export class BandList extends Component {
  state = {
    bands: []
  }

  componentDidMount() {
    if (window.localStorage.getItem('lastSearch')){
    axios
      .get(`${serverURL}/getbands/country/${window.localStorage.getItem('lastSearch')}`)
      .then(res => this.setState({ bands: res.data })); 
    } else {
      axios
        .get(`${serverURL}/getbands`)
        .then(res => this.setState({ bands: res.data }));
    }
  }

  render() {
    if (this.state.bands.length > 0) {
    return this.state.bands.map((band) => {
      return <BandElement key = {band.BandID} band = {band} ></BandElement>
    })
    } else {
      return(<React.Fragment></React.Fragment>)
    }
  }
}

export default BandList
