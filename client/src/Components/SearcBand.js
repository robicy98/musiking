import { BrowserRouter as Router, Route } from 'react-router-dom';
import React, { Component } from 'react';
import BandList from './BandList';

class SearcBand extends Component {
  constructor(props) {
    super(props);

    this.state = {
      search: null,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleChange = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const { state } = this;
    state[event.target.name] = event.target.value;
    this.setState(state);
  }

  handleSubmit() {
    window.localStorage.setItem('lastSearch', document.getElementById('search').value);
    window.location.reload(false);
  }

  render() {
    return (
      <React.Fragment>
        <div className="uk-margin uk-flex uk-flex-right ">
          <form className="uk-search uk-search-default">
            <span uk-search-icon></span>
            <input className="uk-search-input" id = "search" type="text" name = "search" placeholder="Search..."></input>
          </form>
            <input className="uk-button uk-button-primary" type="submit" onClick={this.handleSubmit} value = "Search"></input>
        </div>
        <Route exact path="/" search ={this.state.search} component={BandList}/>
        <Route exact path="/bands" search ={this.state.search} component={BandList}/>
      </React.Fragment>
    );
  }
}

export default SearcBand;
