import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { rules } from '../../props';

class UserLogdIn extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: window.localStorage.getItem('user'),
    };

    this.logOut = this.logOut.bind(this);
  }

  logOut() {
    window.localStorage.clear();
    window.location.reload(false);
  }

  render() {
    if (!localStorage.getItem('user')) {
      return null;
    }
    return (
      <React.Fragment>
        <div className="uk-float-right">
        <button className="uk-button uk-button-default button-no-border" type="button">
          {
            localStorage.getItem('rule') === rules.admin
              ? <Link className = "uk-text-secondary" to="/adminData">{ this.state.username }</Link>
              : <React.Fragment/>
          }
          {
            localStorage.getItem('rule') === rules.player
              ? <Link className = "uk-text-secondary" to="/userData">{ this.state.username }</Link>
              : <React.Fragment/>
          }
          </button>
          <button className="uk-button uk-button-danger button-no-border" type="button" onClick={this.logOut}>
            LogOut
          </button>
        </div>
      </React.Fragment>
    );
  }
}

export default UserLogdIn;
