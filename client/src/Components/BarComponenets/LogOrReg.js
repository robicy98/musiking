import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class LogOrReg extends Component {
  render() {
    if (localStorage.getItem('user')) {
      return null;
    }
    return (
      <React.Fragment>
          <div className="uk-float-right">
            <button className="uk-button uk-button-default button-no-border" type="button">
              <Link className = "uk-text-secondary" to="/loginPage">LogIn</Link>
            </button>
            <button className="uk-button uk-button-primary button-no-border" type="button">
              <Link className = "uk-text-secondary" to="/register">Registration</Link>
            </button>
          </div>
      </React.Fragment>
    );
  }
}

export default LogOrReg;
