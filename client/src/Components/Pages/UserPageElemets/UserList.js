import React, { Component } from 'react'
import { serverURL } from '../../../props'
import UserElement from './UserElement'
const axios = require('axios') 

export class UserList extends Component {

  state = {
    users: []
  }

  componentDidMount() {
    axios
    .get(`${serverURL}/getusers`, { headers: {"Authorization" : `Bearer ${localStorage.getItem('token')}`}})
    .then(res => this.setState({ users: res.data }));
  }

  render() {
    if (this.state.users.length > 0) {
      return (
        this.state.users.map((user) => {
          return <UserElement key = {user.UserID} user = {user}></UserElement>
        })
      )
    } else {
      return (
        <React.Fragment>
        </React.Fragment>
      )
    }
  }
}

export default UserList
