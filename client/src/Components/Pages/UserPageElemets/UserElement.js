import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export class UserElement extends Component {
  render() {
    const linkToUser = `/users/${this.props.user.UserID}`;
    console.log(linkToUser);
    return (
      <div className="uk-card uk-card-default uk-card-body uk-width-1-2@m uk-margin-top margine-left-50">
        <Link to = {linkToUser}>
        <h3  className="uk-card-title">{this.props.user.Firstname} {this.props.user.Lastname}</h3>
        </Link>
        <p>{this.props.user.Rule}</p>
      </div>
    );
  }
}

UserElement.propTypes = {
  user: PropTypes.string,
};

export default UserElement;
