import React, { Component } from 'react';
import axios from 'axios';
import { serverURL } from '../../props';
import UserDetails from './UserDetails';

class UserDataForAdmin extends Component {
  constructor(props) {
    super(props);

    this.state = {
      lastURLSegment: window.location.href.substr(window.location.href.lastIndexOf('/') + 1),
      user: null,
    };
  }

  componentDidMount() {
    axios.get(`${serverURL}/users/${this.state.lastURLSegment}`).then((result) => {
      this.setState({ user: result.data });
    });
  }

  render() {
    if (this.state.user) {
      return (
      <React.Fragment>
        <UserDetails key = {this.state.user.UserID} user={this.state.user} />
      </React.Fragment>
      );
    }
    return (<React.Fragment></React.Fragment>);
  }
}

export default UserDataForAdmin;
