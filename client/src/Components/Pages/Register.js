import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { serverURL } from '../../props';

const axios = require('axios');

class Register extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
      cpassword: '',
      email: '',
      image: '',
      firstname: '',
      lastname: '',
      response: [],
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const { state } = this;
    state[event.target.name] = event.target.value;
    this.setState(state);
  }

  async handleSubmit(event) {
    if (this.state.username !== '') {
      if (this.state.cpassword !== this.state.password) {
        return;
      }
      const emailtest = /[A-Za-z0-9._-]*@[A-Za-z]+\.[a-z]+/;
      if (!emailtest.test(this.state.email)) {
        return;
      }
      axios.get(`${serverURL}/users/${this.state.username}`)
        .then((res) => this.setState({ response: res.data }));
      if (this.state.response.length === 0) {
        const file = null;
        // event.target.files[0];
        console.log(event);
        const d = new Date();
        event.preventDefault();
        const data = new FormData();
        data.append('username', this.state.username);
        data.append('email', this.state.email);
        data.append('password', this.state.password);
        data.append('cpassword', this.state.cpassword);
        data.append('firstname', this.state.firstname);
        data.append('lastname', this.state.lastname);
        if (file) {
          data.append('image', `${window.localStorage.getItem('user')}_
            ${d.getFullYear()}_${d.getMonth() + 1}_${d.getDay() + 1}_
            ${d.getHours()}_${d.getMinutes()}_${d.getSeconds()}`);
          data.append('file', file);
        } else {
          data.append('image', null);
        }
        axios({
          method: 'post',
          url: `${serverURL}/users/`,
          data,
        });
      }
    }
  }

  render() {
    return (
      <React.Fragment>
        <form encType= "multipart/form-data">
        <div className="uk-margin">
            <div className="uk-inline">
              <label> First Name: </label>
              <input className="uk-input" id = "firstname" type="text" name = "firstname" value={this.state.firstname} onChange={this.handleChange}></input>
            </div>
          </div>
          <div className="uk-margin">
            <div className="uk-inline">
              <label> Last Name: </label>
              <input className="uk-input" id = "lastname" type="text" name = "lastname" value={this.state.lastname} onChange={this.handleChange}></input>
            </div>
          </div>
          <div className="uk-margin">
            <div className="uk-inline">
              <label> Username: </label>
              <input className="uk-input" id = "username" type="text" name = "username" value={this.state.username} onChange={this.handleChange}></input>
            </div>
          </div>
          <div className="uk-margin">
            <div className="uk-inline">
              <label> Password: </label>
              <input className="uk-input" id = "password" type="password" name = "password" value={this.state.password} onChange={this.handleChange}></input>
            </div>
          </div>
          <div className="uk-margin">
            <div className="uk-inline">
              <label> Conform Password: </label>
              <input className="uk-input" id = "cpassword" type="password" name = "cpassword" value={this.state.cpassword} onChange={this.handleChange}></input>
            </div>
          </div>
          <div className="uk-margin">
            <div className="uk-inline">
              <label> Email: </label>
              <input className="uk-input" id = "email" type="email" name = "email" value={this.state.email} onChange={this.handleChange}></input>
            </div>
          </div>
          <div className="uk-margin">
            <div className="uk-inline">
              <label> Profile Picture: </label>
              <input type="file" className="uk-input button-no-border" name = "image" multiple value={this.state.image} onChange={this.handleChange}></input>
            </div>
          </div>
          <Link className = "uk-text-secondary" to="/" >
            <button className = "uk-button uk-button-default">
              Cancel
            </button>
          </Link>
        </form>
        <button onClick= {this.handleSubmit}  className = "uk-button uk-button-primary">Submit</button>

      </React.Fragment>
    );
  }
}

export default Register;
