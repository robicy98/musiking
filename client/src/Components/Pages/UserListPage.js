import React, { Component } from 'react';
import UserList from './UserPageElemets/UserList';
import TopBar from '../TopBar';

class UserListPage extends Component {
  render() {
    return (
      <React.Fragment>
        <TopBar/>
        <UserList/>
      </React.Fragment>
    );
  }
}

export default UserListPage;
