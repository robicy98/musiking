import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import TopBar from '../TopBar';
import PropTypes from 'prop-types';

class UserDetails extends Component {
  render() {
    return (
      <React.Fragment>
        <TopBar/>
          <p> FirstName is : {this.props.user.FirstName} </p>
          <p> LastName is : {this.props.user.LastName} </p>
          <p> Rule is : {this.props.user.Rule} </p>
          <p> Email is : {this.props.user.Email} </p>
          <p> Username is : {this.props.user.Username}</p>
        <Link to='/users'>
        <button className="uk-button uk-button-primary uk-flex-last button-no-border">Back</button>
        </Link>
      </React.Fragment>
    );
  }
}

UserDetails.propTypes = {
  user: PropTypes.string,
};

export default UserDetails;
