import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import TopBar from '../TopBar';
import { serverURL } from '../../props';

const axios = require('axios');

class BandData extends Component {
  constructor(props) {
    super(props);

    this.state = {
      lastURLSegment: window.location.href.substr(window.location.href.lastIndexOf('/') + 1),
      band: null,
      bandMaster: null,
      member: null,
      userID: null,
    };

    this.handleLeave = this.handleLeave.bind(this);
    this.handleJoin = this.handleJoin.bind(this);
  }

  handleJoin() {
    const data = new FormData();
    data.append('UserID',  this.state.userID);
    data.append('BandID', this.state.lastURLSegment);
    axios({
      method: 'post',
      url: `${serverURL}/members`,
      data,
    }).then(() => {
      window.location.reload(false);
      this.setState({ member: true });
    });
  }

  handleLeave() {
    const data = new FormData();
    data.append('UserID',  this.state.userID);
    data.append('BandID', this.state.lastURLSegment);
    axios({
      method: 'delete',
      url: `${serverURL}/members`,
      data,
    }).then(() => {
      window.location.reload(false);
      this.setState({ member: false });
    });
  }

  componentDidMount() {
    axios.get(`${serverURL}/users/${localStorage.getItem('user')}`).then((result) => {
      this.setState({ userID: result.data.UserID });
      axios.get(`${serverURL}/members/${result.data.UserID}`).then((member) => {
        for (const element in member.data) {
          if (member.data[element].BandID
              === parseInt(this.state.lastURLSegment, 10)) this.setState({ member: true });
        }
        if (this.state.member === null) {
          this.setState({ member: false });
        }
      });
    });
    axios.get(`${serverURL}/bands/${this.state.lastURLSegment}`).then((result) => {
      this.setState({ band: result.data });
      axios.get(`${serverURL}/users/${result.data.BandMaster}`).then((master) => {
        this.setState({ bandMaster: `${master.data.FirstName} ${master.data.LastName}` });
      });
    });
  }

  render() {
    if (this.state.band) {
      return (
      <React.Fragment>
        <TopBar/>
        <h1> Band Name: { this.state.band.BandName } </h1>
        <h2> Band Master: { this.state.bandMaster} </h2>
        <h3> {this.state.band.Contry} / {this.state.band.City} </h3>
        <h4> {this.state.band.BandType} </h4>
        {
          this.state.member
            ? <button className="uk-button uk-button-danger uk-flex-last button-no-border" onClick={this.handleLeave} >Leave</button>
            : <button className="uk-button uk-button-primary uk-flex-last button-no-border" onClick={this.handleJoin}>Join</button>
        }
        <br/>
        <Link to='/'>
        <button className="uk-button uk-button-danger uk-flex-last button-no-border">Back</button>
        </Link>
      </React.Fragment>
      );
    }
    return (<React.Fragment/>);
  }
}

export default BandData;
