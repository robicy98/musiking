import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { serverURL } from '../../props';

const axios = require('axios');

class LogIn extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
      token: '',
      rule: '',
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const { state } = this;
    state[event.target.name] = event.target.value;
    this.setState(state);
  }

  async handleSubmit() {
    const data = new FormData();
    data.append('username', this.state.username);
    data.append('password', this.state.password);
    axios({
      method: 'post',
      url: `${serverURL}/login/`,
      data,
    }).then((res) => {
      localStorage.setItem('token', res.data);
      axios.get(`${serverURL}/users/${this.state.username}`)
        .then((result) => {
          this.setState({ rule: result.data.Rule });
          localStorage.setItem('rule', this.state.rule);
          localStorage.setItem('user', this.state.username);
        });
    });
  }

  render() {
    if (window.localStorage.getItem('user') && window.localStorage.getItem('user') !== '') {
      return (<Redirect to = "/"/>);
    }
    return (
        <React.Fragment>
          <form>
            <div className="uk-margin">
              <div className="uk-inline">
                <span className="uk-form-icon" uk-icon="icon: user"></span>
                <input className="uk-input" id = "username" type="text" name = "username" value={this.state.username} onChange={this.handleChange}></input>
              </div>
            </div>
            <div className="uk-margin">
              <div className="uk-inline">
                <span className="uk-form-icon" uk-icon="icon: lock"></span>
                <input className="uk-input" id = "password" type="password" name = "password" value={this.state.password} onChange={this.handleChange}></input>
              </div>
            </div>
            <input type='submit' value = 'Submit' onClick={this.handleSubmit} className = "uk-button uk-button-primary"></input>
          </form>
            <Link className = "uk-text-secondary" to="/" >
              <button className = "uk-button uk-button-default">
                Cancel
              </button>
            </Link>
        </React.Fragment>
    );
  }
}

export default LogIn;
