import React, { Component } from 'react';
import { serverURL } from '../../props';
import TopBar from '../TopBar';

const axios = require('axios');

class AdminDetails extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: null,
    };
  }


  componentDidMount() {
    if (localStorage.getItem('user')) {
      axios
        .get(`${serverURL}/getmydata/${localStorage.getItem('user')}`, { headers: { Authorization: `Bearer ${localStorage.getItem('token')}` } })
        .then((res) => this.setState({ user: res.data }));
    }
  }


  render() {
    console.log(this.state.user);
    if (this.state.user) {
      return (
        <React.Fragment>
          <TopBar/>
          <p> FirstName is : {this.state.user[0].FirstName} </p>
          <p> LastName is : {this.state.user[0].LastName} </p>
          <p> Rule is : {this.state.user[0].Rule} </p>
          <p> Email is : {this.state.user[0].Email} </p>
          <p> Username is : {this.state.user[0].Username}</p>
        </React.Fragment>
      );
    }
    return <div></div>;
  }
}

export default AdminDetails;
