import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import LogOrRed from './BarComponenets/LogOrReg';
import UserLogdIn from './BarComponenets/UserLogdIn';
import { rules } from '../props';

class TopBar extends Component {
  render() {
    return (
      <React.Fragment>
        <div className = "uk-background-muted">
          <div className="uk-clearfix">
            <LogOrRed />
            <UserLogdIn/>
          <div className="uk-float-left">
            <button className="uk-button uk-button-default uk-flex-last button-no-border" type="button" uk-toggle="target: #offcanvas-overlay">Menu</button>
          </div>
          </div>
          <div id="offcanvas-overlay" uk-offcanvas="overlay: true">
            <div className="uk-offcanvas-bar">
              <Link to="/">Home</Link><br/>
              {
                localStorage.getItem('rule') === rules.admin ? <Link to = "/users">Users</Link>
                  : <div></div>
              }
            </div>
          </div>
        </div>

      </React.Fragment>
    );
  }
}

export default TopBar;
