import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export class BandElement extends Component {
  render() {
    const bandLink = `/bands/${this.props.band.BandID}`;
    return (
      <div className="uk-card uk-card-default uk-card-body uk-width-1-2@m uk-margin-top margine-left-50">
        <Link to = {bandLink}>
        <h3  className="uk-card-title">{this.props.band.BandName}</h3>
        </Link>
        <p>{this.props.band.Contry}</p>
      </div>
    );
  }
}

BandElement.propTypes = {
  band: PropTypes.string,
};

export default BandElement;
