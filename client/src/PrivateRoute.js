import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Redirect,
} from 'react-router-dom';

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={(props) => (
    localStorage.getItem('rule') === rest.rule
      ? <Component {...props} />
      : <Redirect to='/loginPage'/>
  )} />
);

export default PrivateRoute;
