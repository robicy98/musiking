import React from 'react';
import ReactDOM from 'react-dom';

import { BrowserRouter as Router, Route } from 'react-router-dom';
import TopBar from './Components/TopBar';
import Register from './Components/Pages/Register';
import LogIn from './Components/Pages/LogIn';
import UserListPage from './Components/Pages/UserListPage';
import SearcBand from './Components/SearcBand';
import PrivateRoute from './PrivateRoute';
import AdminDetails from './Components/Pages/AdminDetails';
import UserDataForAdmin from './Components/Pages/UserDataForAdmin';
import BandData from './Components/Pages/BandData';
import { rules } from './props';

import '../node_modules/uikit/dist/css/uikit-core.min.css';

import '../node_modules/uikit/dist/js/uikit-core.min.js';

ReactDOM.render(
  <React.Fragment>
    <Router>
      <Route exact path="/" component={TopBar}/>
      <Route exact path="/" component={SearcBand}/>
      <Route exact path="/bands" component={TopBar}/>
      <Route exact path="/bands" component={SearcBand}/>
      <Route exact path="/bands/*" component={BandData}/>
      <Route exact path="/register" component={Register}/>
      <Route exact path="/loginPage" component={LogIn}/>
      <PrivateRoute exact path="/userData" rule = {rules.player} component={AdminDetails}/>
      <PrivateRoute exact path="/adminData" rule = {rules.admin} component={AdminDetails}/>
      <PrivateRoute exact path="/users" rule = {rules.admin} component={UserListPage}/>
      <PrivateRoute exact path="/users/*" rule = {rules.admin} component={UserDataForAdmin}/>
    </Router>
  </React.Fragment>,
  document.getElementById('root'),
);
